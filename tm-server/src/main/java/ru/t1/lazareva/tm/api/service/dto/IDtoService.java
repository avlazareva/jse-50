package ru.t1.lazareva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;
import ru.t1.lazareva.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoService<M extends AbstractModelDto> extends IDtoRepository<M> {

    @NotNull
    M add(@Nullable M model);

    @NotNull Collection<M> add(@Nullable Collection<M> models);

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void removeByIndex(@Nullable Integer index) throws AbstractException;

}

