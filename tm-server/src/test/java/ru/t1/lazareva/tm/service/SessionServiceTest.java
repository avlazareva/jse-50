package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.*;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.service.dto.ProjectDtoService;
import ru.t1.lazareva.tm.service.dto.SessionDtoService;
import ru.t1.lazareva.tm.service.dto.TaskDtoService;
import ru.t1.lazareva.tm.service.dto.UserDtoService;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.ProjectTestData.USER_1;
import static ru.t1.lazareva.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.lazareva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IDtoService DTO_SERVICE = new SessionDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final ISessionDtoService SERVICE = new SessionDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskDtoService TASK_SERVICE = new TaskDtoService(CONNECTION_SERVICE);

    @NotNull
    private static final IProjectDtoService PROJECT_SERVICE = new ProjectDtoService(CONNECTION_SERVICE);

    private static final IUserDtoService USER_SERVICE = new UserDtoService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        USER_SERVICE.add(USER_1);
        USER_SERVICE.add(USER_2);
        SERVICE.add(USER_1.getId(), USER_1_SESSION);
        SERVICE.add(USER_2.getId(), USER_2_SESSION);
    }

    @After
    public void after() throws Exception {
        SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Test
    public void add() throws Exception {
        SERVICE.clear();
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.add(NULL_SESSION);
        });
        SERVICE.clear();
        Assert.assertNotNull(SERVICE.add(USER_1_SESSION));
        @Nullable final SessionDto session = SERVICE.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.add(USER_1.getId(), NULL_SESSION);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_1_SESSION);
        });
        SERVICE.clear();
        Assert.assertNotNull(SERVICE.add(USER_1.getId(), USER_1_SESSION));
        @Nullable final SessionDto session = SERVICE.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<SessionDto> sessions = SERVICE.findAll();
        Assert.assertNotNull(sessions);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(""));
        Assert.assertFalse(SERVICE.existsById(null));
        Assert.assertFalse(SERVICE.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(SERVICE.existsById(USER_1_SESSION.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.existsById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.existsById(USER_1.getId(), "");
        });
        Assert.assertFalse(SERVICE.existsById(USER_1.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(SERVICE.existsById(USER_1.getId(), USER_1_SESSION.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.findOneById(NON_EXISTING_SESSION_ID);
        });
        @Nullable final SessionDto session = SERVICE.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(USER_1.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, USER_1_SESSION.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", USER_1_SESSION.getId());
        });
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final SessionDto session = SERVICE.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void clear() throws Exception {
        SERVICE.clear();
        Assert.assertEquals(0, SERVICE.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear("");
        });
        SERVICE.clear();
        SERVICE.add(USER_1.getId(), USER_1_SESSION);
        SERVICE.clear(USER_1.getId());
        Assert.assertEquals(0, SERVICE.getSize(USER_1.getId()));
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(null);
        });
        SERVICE.clear();
        @Nullable final SessionDto createdSession = SERVICE.add(USER_1_SESSION);
        SERVICE.remove(createdSession);
        Assert.assertEquals(0, SERVICE.getSize());
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.remove("", null);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            SERVICE.remove(USER_1.getId(), null);
        });
        SERVICE.clear();
        @Nullable final SessionDto createdSession = SERVICE.add(USER_1_SESSION);
        SERVICE.remove(USER_1.getId(), createdSession);
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(USER_1.getId(), "");
        });
        SERVICE.removeById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_1_SESSION.getId()));
        @Nullable final SessionDto createdSession = SERVICE.add(USER_1_SESSION);
        SERVICE.removeById(USER_1.getId(), createdSession.getId());
        Assert.assertNull(SERVICE.findOneById(USER_1.getId(), USER_1_SESSION.getId()));
    }

    @Test
    public void getSize() throws Exception {
        SERVICE.clear();
        SERVICE.add(USER_1_SESSION);
        Assert.assertEquals(1, SERVICE.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize("");
        });
        SERVICE.clear();
        Assert.assertTrue(SERVICE.findAll().isEmpty());
        Assert.assertEquals(0, SERVICE.getSize(USER_1.getId()));
        SERVICE.add(USER_1_SESSION);
        Assert.assertEquals(1, SERVICE.getSize(USER_1.getId()));
    }

}